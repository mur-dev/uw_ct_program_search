<?php
/**
 * @file
 * uw_ct_program_search.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_ct_program_search_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: node
  $overrides["node.uw_ct_program_search.has_title"] = 1;
  $overrides["node.uw_ct_program_search.help"] = '';
  $overrides["node.uw_ct_program_search.name"] = 'Program Search';

 return $overrides;
}
