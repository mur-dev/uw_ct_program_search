<?php
/**
 * @file
 * uw_ct_program_search.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_program_search_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info_alter().
 */
function uw_ct_program_search_node_info_alter(&$data) {
  if (isset($data['uw_ct_program_search'])) {
    $data['uw_ct_program_search']['has_title'] = 1; /* WAS: TRUE */
    $data['uw_ct_program_search']['help'] = ''; /* WAS: '' */
    $data['uw_ct_program_search']['name'] = 'Program Search'; /* WAS: 'Program Search Type' */
  }
}
