<?php
/**
 * @file
 * uw_ct_program_search.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_program_search_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_ct_program_search content'.
  $permissions['create uw_ct_program_search content'] = array(
    'name' => 'create uw_ct_program_search content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_ct_program_search content'.
  $permissions['delete any uw_ct_program_search content'] = array(
    'name' => 'delete any uw_ct_program_search content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_ct_program_search content'.
  $permissions['delete own uw_ct_program_search content'] = array(
    'name' => 'delete own uw_ct_program_search content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_ct_program_search content'.
  $permissions['edit any uw_ct_program_search content'] = array(
    'name' => 'edit any uw_ct_program_search content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_ct_program_search content'.
  $permissions['edit own uw_ct_program_search content'] = array(
    'name' => 'edit own uw_ct_program_search content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  return $permissions;
}
